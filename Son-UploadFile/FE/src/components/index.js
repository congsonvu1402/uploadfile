import React from "react";
import { saveAs } from 'file-saver'
export default class Index extends React.Component {
    state = {
        name: '',
        fileImg: [],
        linkImg: [],
        id: '',
        nameUpdate: '',
        nameSearch: '',

    }
    handleReview(file) {
        this.setState({ fileImg: file })
        let arrImg = []
        for (let i = 0; i < file.length; i++) {
            arrImg.push(URL.createObjectURL(file[i]))
        }
        this.setState({ linkImg: arrImg })
    }
    downloadImage = () => {
        saveAs('image_url', 'image.jpg') // Put your image url here.
    }

    render() {
        let listData = []
        if (this.props.itemFile) {
            listData = this.props.itemFile.map((item, key) => {
                return (
                    <tr key={item._id}>

                        <th>{key + 1}</th>
                        <th>{item.name}</th>
                        <th>{item.img.map((value, key) => {
                            return (
                                <div key={key}>
                                    <img alt="error images" src={value} width={90} height={90}></img>
                                    <button onClick={() => item.img.length > 1 ?
                                        this.props.deleteOne({ index: key, id: item._id }) :
                                        this.props.deleteItem({ id: item._id })
                                    }>x</button>
                                </div>
                            )
                        })}</th>
                        <th><button onClick={() => this.props.deleteItem({ id: item._id })}>DELETE</button></th>
                        <th><button onClick={() => { this.setState({ id: item._id, linkImg: item.img, nameUpdate: item.name }) }}>Choose</button></th>
                        <th>
                            <button onClick={this.downloadImage}>DownLoad</button>
                        </th>
                    </tr>
                )
            })
        }
        return (
            <div>
                <div>{this.state.linkImg.map((link, index) => {
                    return (
                        <div key={index}>
                            <span>
                                <img src={link} alt='error images' width={90} heigth={90}></img>
                            </span>
                        </div>
                    )
                })}</div>
                <input multiple type='file' onChange={(e) => { this.handleReview(e.target.files) }}></input>
                <br></br>
                <input type='text' onChange={(e) => this.setState({ name: e.target.value })}></input>
                <button

                    onClick={() => {
                        this.props.addItem({
                            name: this.state.name,
                            img: this.state.fileImg
                        })
                    }}>submit</button>
                <br></br>
                <input type='text' onChange={(e) => { this.setState({ nameSearch: e.target.value }) }}></input>
                <button onClick={() => { this.props.searchItem({ nameSearch: this.state.nameSearch }) }}>Search</button>
                <br></br>
                <input value={this.state.nameUpdate} onChange={(e) => { this.setState({ nameUpdate: e.target.value }) }}></input>
                <button onClick={() => {
                    this.props.updateItem({
                        id: this.state.id,
                        img: this.state.fileImg,
                        name: this.state.nameUpdate
                    })
                }}>Update</button>
                <table>
                    <tbody>
                        <tr>
                            <th>Id</th>
                            <th>Ten</th>
                            <th>Anh</th>
                        </tr>
                        {listData}
                    </tbody>
                </table>
            </div>
        )
    }
}
