import * as types from '../constants'
const DEFAULT_STATE = {
    listData: [],
    isFetching: true,
    dataFetched: false,
    error: false,
    errorMessage: null,
  
}
export default (state = DEFAULT_STATE, actions) => {
    switch (actions.type) {
        case types.ADD_ITEM_REQUEST:
        case types.DELETE_ITEM_REQUEST:
        case types.UPDATE_ITEM_REQUEST:
        case types.GET_ITEM_REQUEST:
        case types.DELETE_ONE_REQUEST:
        case types.SEARCH_ITEM_REQUEST:

            return {
                ...state,
                isFetching: true,
                dataFetched: false
            }
        case types.ADD_ITEM_SUCCESS:
        case types.DELETE_ITEM_SUCCESS:
        case types.UPDATE_ITEM_SUCCESS:
        case types.DELETE_ONE_SUCCESS:
       
            return {
                ...state,
                isFetching: false,
                dataFetched: true,
                error: false,
                errorMessage: null,
            }
        case types.GET_ITEM_SUCCESS:
            case types.SEARCH_ITEM_SUCCESS:
            return {
                ...state,
                isFetching: false,
                dataFetched: true,
                error: false,
                errorMessage: null,
                listData: actions.payload.res,
            }
        case types.ADD_ITEM_FAILURE:
        case types.DELETE_ITEM_FAILURE:
        case types.UPDATE_ITEM_FAILURE:
        case types.GET_ITEM_FAILURE:
        case types.DELETE_ONE_FAILURE:
        case types.SEARCH_ITEM_FAILURE:
            return {
                ...state,
                isFetching: false,
                dataFetched: false,
                error: true,
                errorMessage: 'loi roi'
            }
        default:
            return state
    }
}