const ItemModel = require("../model/index")
const fs = require('fs')

const getItem = async (req, res) => {
    try {
        let data = await ItemModel.find()
        res.send({ data })
    } catch (error) {
        res.send({ message: error.message })
    }
}
const addItem = async (req, res) => {
    try {
        let name = req.body.name
        const fileImg = req.files
        const arrImg = []
        for (let i = 0; i < fileImg.length; i++) {
            const url = `http://localhost:3001/${fileImg[i].filename}`
            arrImg.push(url)
        }
        const response = await ItemModel.create({ name, img: arrImg, time: Date.now() })
        res.send({ data: response })

    } catch (error) {
        res.send({ message: error.message })
    }
}

const deleteItem = async (req, res) => {
    try {
        let id = req.params.id
        let data = await ItemModel.findByIdAndDelete(id)
        let oldUrl = data.img
        console.log(oldUrl, 'h');
        for (let i = 0; i < oldUrl.length; i++) {
            fs.unlink(`Media/${oldUrl[i].slice(22)}`, () => { })
        }
        res.send({})
    } catch (error) {
        res.send({ message: error.message })
    }
}
const updateItem = async (req, res) => {
    try {
        let id = req.params.id
        let arrImg = []
        let name = req.body.name
        let images = req.files
        let oldImg = await ItemModel.findById(id)
        if (images.length === 0) {
            await findByIdAndUpdate(id, { name: name, img: oldImg.img })
        } else {
            for (let i = 0; i < images.length; i++) {
                arrImg.push(`http://localhost:3001/${images[i].filename}`)
            }
            await ItemModel.findByIdAndUpdate(id, { img: arrImg, name })
        }
        for (let i = 0; i < data.img.length; i++) {
            fs.unlink(`Media/${data.img[i].slice(22)}`, () => { })
        }
        res.send({
            data,
            message: 'success'
        })
    } catch (error) {
        res.send({
            message: error.message
        })
    }
}

const deleteOne = async (req, res) => {
    try {
        let id = req.query.id
        let index = req.query.index
        const oldUrl = await ItemModel.findById(id)
        const img2 = oldUrl.img
        fs.unlink(`Media/${oldUrl.img[index].slice(22)}`, () => { })
        img2.splice(index, 1)
        let data = await ItemModel.findByIdAndUpdate(id, { img: oldUrl.img })
        res.send({})
    } catch (error) {
        res.send({
            message: error.message
        })
    }
}

const searchItem = async (req, res) => {
    try {
        let nameSearch = req.query.nameSearch
        const search = await ItemModel.find({ name: { $regex: nameSearch, $options: 'i' } }).sort({ time: -1 })
        res.send({
            search
        })
    } catch (error) {
        res.send({})
    }
}

module.exports = { getItem, addItem, deleteItem, updateItem, deleteOne, searchItem }